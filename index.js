/*
 * Push Notification
 * Author: Khurram Ijaz
 * Copyright: Nvolv.co
 */
var express = require('express');
var bodyParser = require('body-parser');
var MongoClient = require('mongodb').MongoClient;
var DB = {};
//var db_url = "mongodb://napi:326b2b41be262469844d33039648299a@lighthouse.2.mongolayer.com:10266,lighthouse.3.mongolayer.com:10265/nvolv3?replicaSet=set-55389abe7570be60d70007fd";
var db_url = "mongodb://apidev.nvolv3.com:27017/mobile";

var getMongoDB = function(){
    return new Promise(function(resolve,reject){
           MongoClient.connect(db_url, function(err, db) {
            DB = db;
            resolve(db);
           console.log("Connected to MongoDB");
        });
    })
}   

var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var ObjectId = function(id){
    var obj_id = require('mongodb').ObjectID;
    return new obj_id(id);
}

app.post('/Login', function (req, res) {
    
   var userName = req.body.username ;
   var passworrdstr = req.query.password;
   if(userName.length !=0){
          var getUser = function(db){
        return new Promise(function(resolve,reject){
          db.collection('users').findOne({userName:userName},function(err,data){
                if(data != null){
                     res.json({code:"201",error:"false",message:"Login Successfully",user:{
                      userId:data._id,
                      firstName:"",
                      lastName:"",
                      address:"",
                      contact:"",
                      email:data.userEmail,
                      dob:"",
                      gender:"",
                      occupation:"",
                      image:""
              }});  
                }else{
                 res.json({code:"400",message:"no record found"});   
                }
            
        });
    });
    }  
   }else{
       res.json({error:"400",message:"required field "});
   } 

    
  getMongoDB()
    .then(getUser)
    
    .catch(function(e){
        console.log('Error ',e);
    });
});


app.post('/RegisterUser', function (req, res) {

  var userEmail = req.body.email;
  var userName = req.body.userName;
  var userdeviceID = req.body.deviceID;
  var userdeviceType = req.body.deviceType;
  var passworrdstr = req.body.password;
  if(userEmail.length !=0 && userName.length !=0 && userdeviceID.length !=0 && userdeviceType.length !=0 && passworrdstr.length !=0){
      var crypto = require('crypto');
var hashPassword = crypto.createHash('md5').update(passworrdstr).digest('hex');
var dataArray = {"userName":userName, "userEmail":userEmail, "userPassword": hashPassword,"device_id":userdeviceID,"device_type":userdeviceType};
    var insertUser = function(db){
        return new Promise(function(resolve,reject){
            
            db.collection('users').findOne({userName:userName},function(err,data){
                if(data != null){
                     res.json({code:"409",error:"true",message:"Sorry, this user is already registered"});  
                }else{
            db.collection('users').insert(dataArray,function(err,entry){
                if(entry != null){
                       res.json({code:"201",error:"false",message:"Registration Successfully",user:{
                      userId:entry.ops[0]._id,
                      firstName:entry.ops[0].userName,
                      lastName:"",
                      address:"",
                      contact:"",
                      email:entry.ops[0].userEmail,
                      dob:"",
                      gender:"",
                      occupation:"",
                      image:""
              }});  
            } else{
             res.json({error:"500",message:"Oops! An error occurred while registering"});      
            }   
            
        });
                }
            
        });
            

    });
    }
  }else{
      res.json({error:"400",message:"required field "});
  }

   getMongoDB()
    .then(insertUser)
    
    .catch(function(e){
        console.log('Error ',e);
    });
});


app.post('/UpdateUserProfile', function (req, res) {
 
  var userId = 	req.body.userId; 
  var firstName = req.body.firstName;
  var lastName = req.body.lastName;
  var address = req.body.address;
  var contactNumber = req.body.contactNo;
  var date_of_bith = req.body.dob;
  var gender= req.body.gender;
  var occupation = req.body.occupation;
  var image = req.body.image;
  var userEmail = req.body.email;
  
  if(userId.length !=0){
      var dataArray = {"firstNAme":firstName, "lastName":lastName, "address": address,"number":contactNumber,"dob":date_of_bith,"gender":gender,"occupation":occupation,"image":image,"userEmail":userEmail};
    var UpdateUser = function(db){
        return new Promise(function(resolve,reject){
            
       db.collection('users').update({"_id": ObjectId(userId)},{$set:dataArray},function(err,data){
           if(err){
              res.json({code:"500",message:"Oops! An error occurred while registering"}); 
               
           }else{
               res.json({code:"201",error:"false",message:"Profile Updated Successfully"});
           }
       });
    });
    }
  }else{
      res.json({code:"400",message:"Required Field Error"}); 
  }


   getMongoDB()
    .then(UpdateUser)
    
    .catch(function(e){
        console.log('Error ',e);
    });
});

// change password 

app.post('/ChangePassword', function (req, res) {
 
  var userId = 	req.body.userId; 
  var passworrdstr = req.body.password;
  var crypto = require('crypto');
  var hashPassword = crypto.createHash('md5').update(passworrdstr).digest('hex');
  var userdeviceID = req.body.deviceID;
  var userdeviceType = req.body.deviceType;
 if(userId.length !=0 && passworrdstr.length !=0 && userdeviceID.length !=0 && userdeviceType.length !=0){
         var UpdateUser = function(db){
        return new Promise(function(resolve,reject){
            
       db.collection('users').update({"_id": ObjectId(userId)},{$set: {userPassword: hashPassword},},function(err,data){
           if(err){
               
           }else{
                res.json({code:"201",error:"false",message:"Password Updated Successfully"});
           }
       });
    });
    }
 }else{
     res.json({code:"400",message:"Required Field Error"}); 
 } 


   getMongoDB()
    .then(UpdateUser)
    
    .catch(function(e){
        console.log('Error ',e);
    });
});

//
var config = {
    port: 7003,
    domainName: 'localhost'
};

app.listen(config.port,config.domainName, function () {
  console.log('Nvolv Push Notification Service is Listening at ',config.domainName,':',config.port);
});